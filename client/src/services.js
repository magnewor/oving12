// @flow
import axios from 'axios';

axios.interceptors.response.use(response => response.data);

export class News {
  id: number;
  title: string;
  content: string;
  image: string;
  category: string;
  important: boolean;
  createdAt: any;
}

export class Category {
  name: string;
}

export class Comment {
  id: number;
  nickname: string;
  content: string;
  newsId: number;
  createdAt: any;
}

export let news = new News();
export let category = new Category();
export let comment = new Comment();

class NewsService {
  getNews(): Promise<News[]> {
    return axios.get('/news');
  }
  getCategoryNews(category: string): Promise<News[]> {
    return axios.get('/news/' + category);
  }
  getOneNews(id: number, category: string): Promise<News> {
    return axios.get('/news/' + category + '/' +  id);
  }
  addNews(news: News): Promise<News> {
    return axios.post('/news', news);
  }
  delNews(id: number, category: string): Promise<void> {
    return axios.delete('/news/' + category + '/' +  id);
  }
  updateNews(news: News): Promise<void> {
    return axios.put('/news/' + news.category + '/' + news.id, news);
  }
}

class CategoryService {
  getCategories(): Promise<Category[]> {
    return axios.get('/categories');
  }
}

class CommentsService {
  getCommentsNews(id: number): Promise<Comment[]> {
    return axios.get('/comments/' + id)
  }
  addComment(comment: Comment): Promise<Comment> {
    return axios.post('/comments/', comment)
  }
}

export let newsService = new NewsService();
export let categoryService = new CategoryService();
export let commentsService = new CommentsService();
