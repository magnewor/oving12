// @flow
/* eslint eqeqeq: "off" */

import * as React from 'react';
import { Component } from 'react-simplified';
import { Collapse, Navbar, NavbarToggler, Nav, NavItem, NavLink } from 'reactstrap';
import { NavLink as RRNavLink } from 'react-router-dom';
import { newsService, News, Category, Comment } from './services';
import createHashHistory from 'history/createHashHistory';
const history = createHashHistory();
const dateFormat = require('dateformat');

export class AutoCollapsingNavbar extends Component<{ categories: Category[] }> {
  constructor(props: any) {
    super(props);

    // $FlowFixMe
    this.toggle = this.toggle.bind(this);
    // $FlowFixMe
    this.state = {
      isOpen: false
    };
  }

  toggle() {
    // $FlowFixMe
    this.setState({
      // $FlowFixMe
      isOpen: !this.state.isOpen
    });
  }

  render() {
    return (
      <div>
        <Navbar navbar dark expand="md">
          <Nav navbar className="mr-auto">
            <NavItem className="navItem">
              <NavLink tag={RRNavLink} className="nav-link" exact to="/" activeClassName="active">
                Hjem
              </NavLink>
            </NavItem>
          </Nav>
          <NavbarToggler onClick={this.toggle} />
          <Collapse isOpen={
            // $FlowFixMe
            this.state.isOpen} navbar>
            <Nav navbar>
              {this.props.categories.map(category => (
                <NavItem className="navItem" key={category.name}>
                  <NavLink
                    tag={RRNavLink}
                    className="nav-link"
                    to={'/news/' + category.name.toLocaleLowerCase()}
                    activeClassName="active"
                  >
                    {category.name}
                  </NavLink>
                </NavItem>
              ))}
            </Nav>
            <Nav className="ml-auto" navbar>
              <NavItem className="navItem">
                <NavLink tag={RRNavLink} className="nav-link" to="/news/new" activeClassName="active">
                  Registrer Nyhet
                </NavLink>
              </NavItem>
            </Nav>
          </Collapse>
        </Navbar>
      </div>
    );
  }
}

export class NewsView extends Component<{ news: News, comments: Comment[], addmethod: any }> {
  render() {
    return (
      <div>
        <div className="container">
          <h1>{this.props.news.title}</h1>
          <img className="img-thumbnail" src={this.props.news.image} />
          <p className="contentcontainer">{this.props.news.content}</p>
        </div>
        <div className="container">
          <p className="font-weight-bold">Kommentarer:</p>
          <form id="inputForm">
            <div className="form-group">
              <input
                type="text"
                className="form-control"
                id="nicknameInput"
                placeholder="Skriv kallenavn her..."
                required
              />
            </div>
            <div className="form-group">
              <textarea
                className="form-control vresize"
                id="contentInput"
                required
                placeholder="Skriv kommentar her..."
              />
            </div>
            <div className="form-group">
              <input
                onClick={this.props.addmethod}
                type="submit"
                className="form-control"
                value="Kommenter"
                placeholder="Another input"
                id="commentButton"
              />
            </div>
          </form>
          <ul className="list-unstyled">
            {this.props.comments.map(comment => (
              <li key={comment.id}>
                <p>
                  {comment.nickname} {dateFormat(comment.createdAt, '- HH:MM, d/m/yy')}: {comment.content}
                </p>
              </li>
            ))}
          </ul>
        </div>
        <div className="editanddelete container">
          <a
            className="btn btn-primary"
            href={'#/news/' + this.props.news.category + '/' + this.props.news.id + '/edit'}
            role="button"
          >
            Endre
          </a>
          <a
            className="btn btn-danger"
            href={'#/news/' + this.props.news.category + '/' + this.props.news.id + '/delete'}
            role="button"
          >
            Slett
          </a>
        </div>
      </div>
    );
  }
}

export class NewsList extends Component<{ news: News[], home?: boolean }> {
  render() {
    return (
      <div className={this.props.home ? 'container' : 'categoriesc container'}>
        <div className="row">
          {this.props.news.map(news => (
            <div className="col-lg-4 col-sm-6" key={news.id}>
              <RRNavLink exact to={'/news/' + news.category.toLocaleLowerCase() + '/' + news.id}>
                <img className="img-thumbnail" src={news.image} />
              </RRNavLink>
              <RRNavLink
                activeStyle={{ color: 'darkblue' }}
                exact
                to={'/news/' + news.category.toLocaleLowerCase() + '/' + news.id}
              >
                {news.title}
              </RRNavLink>
            </div>
          ))}
        </div>
      </div>
    );
  }
}

export class NewsNewForm extends Component<{ categories: Category[] }> {
  render() {
    return (
      <div className="form-container">
        <form id="inputForm">
          <div className="form-group">
            <label htmlFor="titleInput">Overskrift</label>
            <input
              type="text"
              className="form-control"
              id="titleInput"
              placeholder="Skriv inn en overskrift her..."
              required
            />
          </div>
          <div className="form-group">
            <label htmlFor="contentInput">Innhold</label>
            <textarea
              className="form-control vresize"
              id="contentInput"
              required
              placeholder="Skrive inn innhold her..."
            />
          </div>
          <div className="form-group">
            <label htmlFor="imageInput">Bilde</label>
            <input
              type="text"
              className="form-control"
              id="imageInput"
              placeholder="https://..."
              pattern="https?://.+"
              required
            />
          </div>
          <label>Kategori</label>
          <select className="form-control" id="categoryInput">
            {this.props.categories.map(category => (
              <option value={category.name} key={category.name}>
                {category.name}
              </option>
            ))}
          </select>
          <div className="form-check">
            <input className="form-check-input" type="checkbox" value="" id="defaultCheck1" />
            <label className="form-check-label" htmlFor="defaultCheck1">
              Viktig
            </label>
          </div>
          <div className="form-group">
            <input
              onClick={this.add}
              type="submit"
              className="form-control"
              value="Publiser"
              placeholder="Another input"
            />
          </div>
        </form>
      </div>
    );
  }

  add() {
    let form = document.getElementById('inputForm');
    // $FlowFixMe
    if (form.checkValidity()) {
      newsService
        // $FlowFixMe
        .addNews({
          // $FlowFixMe
          title: document.getElementById('titleInput').value,
          // $FlowFixMe
          content: document.getElementById('contentInput').value,
          // $FlowFixMe
          image: document.getElementById('imageInput').value,
          // $FlowFixMe
          category: document.getElementById('categoryInput').value,
          // $FlowFixMe
          important: document.getElementById('defaultCheck1').checked
        })
        .then(news => history.push('/news/' + news.category + '/' + news.id));
    } else {
      console.log('Form not valid');
    }
  }
}

export class NewsEditForm extends Component<{ news: News, categories: Category[], savemethod: any }> {
  render() {
    return (
      <div className="form-container">
        <form id="inputForm">
          <div className="form-group">
            <label htmlFor="titleInput">Overskrift</label>
            <input
              type="text"
              className="form-control"
              id="titleInput"
              placeholder="Skriv inn en overskrift her..."
              required
              value={this.props.news.title}
              onChange={(event: SyntheticInputEvent<HTMLInputElement>) => {
                if (this.props.news) this.props.news.title = event.target.value;
              }}
            />
          </div>
          <div className="form-group">
            <label htmlFor="contentInput">Innhold</label>
            <textarea
              className="form-control vresize"
              id="contentInput"
              required
              placeholder="Skrive inn innhold her..."
              value={this.props.news.content}
              onChange={(event: SyntheticInputEvent<HTMLInputElement>) => {
                if (this.props.news) this.props.news.content = event.target.value;
              }}
            />
          </div>
          <div className="form-group">
            <label htmlFor="imageInput">Bilde</label>
            <input
              type="text"
              className="form-control"
              id="imageInput"
              placeholder="https://..."
              pattern="https?://.+"
              required
              value={this.props.news.image}
              onChange={(event: SyntheticInputEvent<HTMLInputElement>) => {
                if (this.props.news) this.props.news.image = event.target.value;
              }}
            />
          </div>
          <label>Kategori</label>
          <select
            className="form-control"
            id="categoryInput"
            defaultValue={this.props.news.category}
            onChange={(event: SyntheticInputEvent<HTMLInputElement>) => {
              if (this.props.news) this.props.news.category = event.target.value;
            }}
          >
            {this.props.categories.map(category => (
              <option value={category.name} key={category.name}>
                {category.name}
              </option>
            ))}
          </select>
          <div className="form-check">
            <input
              className="form-check-input"
              type="checkbox"
              value=""
              id="defaultCheck1"
              checked={this.props.news.important}
              onChange={() => {
                if (this.props.news) this.props.news.important = !this.props.news.important;
              }}
            />
            <label className="form-check-label" htmlFor="defaultCheck1">
              Viktig
            </label>
          </div>
          <div className="form-group">
            <input
              onClick={this.props.savemethod}
              type="submit"
              className="form-control"
              value="Oppdater"
              placeholder="Another input"
            />
          </div>
        </form>
      </div>
    );
  }
}

export class LiveFeed extends Component<{ news: News[] }> {
  render() {
    return (
      <marquee>
        {this.props.news.slice(0, 10).map(news => (
          <RRNavLink
            activeStyle={{ color: 'darkblue' }}
            exact
            to={'/news/' + news.category.toLocaleLowerCase() + '/' + news.id}
            key={news.id}
          >
            {news.title + ' - ' + dateFormat(news.createdAt, 'HH:MM, d/m/yy') + ' '}
          </RRNavLink>
        ))}
      </marquee>
    );
  }
}

export class DeleteConfirmation extends Component<{ news: News, deletemethod: any }> {
  render() {
    return (
      <div className="confirm">
        <p>Er du sikker?</p>
        <div>
          <button className="btn btn-success" onClick={this.props.deletemethod}>
            Ja
          </button>
          <a
            className="btn btn-danger"
            href={'#/news/' + this.props.news.category + '/' + this.props.news.id}
            role="button"
          >
            Nei
          </a>
        </div>
      </div>
    );
  }
}

/**
 * Renders alert messages using Bootstrap classes.
 */
export class Alert extends Component {
  alerts: { text: React.Node, type: string }[] = [];

  render() {
    return (
      <>
        {this.alerts.map((alert, i) => (
          <div key={i} className={'alert alert-' + alert.type} role="alert">
            {alert.text}
            <button
              className="close"
              onClick={() => {
                this.alerts.splice(i, 1);
              }}
            >
              &times;
            </button>
          </div>
        ))}
      </>
    );
  }

  static success(text: React.Node) {
    // To avoid 'Cannot update during an existing state transition' errors, run after current event through setTimeout
    setTimeout(() => {
      for (let instance of Alert.instances()) instance.alerts.push({ text: text, type: 'success' });
    });
  }

  static info(text: React.Node) {
    // To avoid 'Cannot update during an existing state transition' errors, run after current event through setTimeout
    setTimeout(() => {
      for (let instance of Alert.instances()) instance.alerts.push({ text: text, type: 'info' });
    });
  }

  static warning(text: React.Node) {
    // To avoid 'Cannot update during an existing state transition' errors, run after current event through setTimeout
    setTimeout(() => {
      for (let instance of Alert.instances()) instance.alerts.push({ text: text, type: 'warning' });
    });
  }

  static danger(text: React.Node) {
    // To avoid 'Cannot update during an existing state transition' errors, run after current event through setTimeout
    setTimeout(() => {
      for (let instance of Alert.instances()) instance.alerts.push({ text: text, type: 'danger' });
    });
  }
}
