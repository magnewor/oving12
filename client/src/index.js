// @flow

import ReactDOM from 'react-dom';
import * as React from 'react';
import { Component } from 'react-simplified';
import { HashRouter, Route } from 'react-router-dom';
import {
  Alert,
  AutoCollapsingNavbar,
  DeleteConfirmation,
  LiveFeed,
  NewsNewForm,
  NewsList,
  NewsView,
  NewsEditForm
} from './widgets';
import { categoryService, newsService, commentsService, news, News, Category, Comment } from './services';

// Reload application when not in production environment
if (process.env.NODE_ENV !== 'production') {
  let script = document.createElement('script');
  script.src = '/reload/reload.js';
  if (document.body) document.body.appendChild(script);
}

import createHashHistory from 'history/createHashHistory';
const history = createHashHistory();

class Menu extends Component {
  categories: Category[] = [];

  render() {
    return <AutoCollapsingNavbar categories={this.categories} />;
  }

  mounted() {
    categoryService
      .getCategories()
      .then(categories => (this.categories = categories))
      .catch((error: Error) => Alert.danger(error.message));
  }
}

class Home extends Component {
  news: News[] = [];

  render() {
    return (
      <div>
        <LiveFeed news={this.news} />
        <NewsList news={this.news.filter(news => news.important).slice(0, 21)} home />
      </div>
    );
  }

  mounted() {
    newsService
      .getNews()
      .then(news => (this.news = news))
      .catch((error: Error) => Alert.danger(error.message));
  }
}

class NewsDetails extends Component<{ match: { params: { id: number, category: string } } }> {
  news: News = news;
  comments: Comment[] = [];

  render() {
    return <NewsView news={this.news} comments={this.comments} addmethod={this.add} />;
  }

  mounted() {
    newsService
      .getOneNews(this.props.match.params.id, this.props.match.params.category)
      .then(news => (this.news = news))
      .catch((error: Error) => Alert.danger(error.message));
    commentsService
      .getCommentsNews(this.props.match.params.id)
      .then(comments => (this.comments = comments))
      .catch((error: Error) => Alert.danger(error.message));
  }

  add() {
    let form = document.getElementById('inputForm');
    // $FlowFixMe
    if (form.checkValidity()) {
      let comment = new Comment();
      let nicknameInput = document.getElementById('nicknameInput');
      let contentInput = document.getElementById('contentInput');

      // $FlowFixMe
      comment.nickname = nicknameInput.value;
      // $FlowFixMe
      comment.content = contentInput.value;
      comment.newsId = this.news.id;

      commentsService.addComment(comment).then(() => {
        // $FlowFixMe
        nicknameInput.value = '';
        // $FlowFixMe
        contentInput.value = '';
        this.mounted();
      });
    } else {
      console.log('Form not valid');
    }
  }
}

class NewsEdit extends Component<{ match: { params: { id: number, category: string } } }> {
  categories: Category[] = [];
  news: News = news;

  render() {
    if (this.categories.length === 0) return null;
    return <NewsEditForm news={this.news} categories={this.categories} savemethod={this.save} />;
  }

  mounted() {
    categoryService
      .getCategories()
      .then(categories => (this.categories = categories))
      .catch((error: Error) => Alert.danger(error.message));
    newsService
      .getOneNews(this.props.match.params.id, this.props.match.params.category)
      .then(news => (this.news = news))
      .catch((error: Error) => Alert.danger(error.message));
  }

  save() {
    let form = document.getElementById('inputForm');
    // $FlowFixMe
    if (form.checkValidity()) {
      newsService
        .updateNews(this.news)
        .then(() => {
          // $FlowFixMe
          if (NewsDetails.instance()) NewsDetails.instance().mounted();
          history.push('/news/' + this.news.category + '/' + this.props.match.params.id);
        })
        .catch((error: Error) => Alert.danger(error.message));
    }
  }
}

class NewsDelete extends Component<{ match: { params: { id: number, category: string } } }> {
  news: News = news;

  render() {
    if (!this.news) return null;
    return <DeleteConfirmation news={this.news} deletemethod={this.delete} />;
  }

  mounted() {
    newsService
      .getOneNews(this.props.match.params.id, this.props.match.params.category)
      .then(news => (this.news = news))
      .catch((error: Error) => Alert.danger(error.message));
  }

  delete() {
    newsService
      .delNews(this.news.id, this.props.match.params.category)
      .then(() => {
        // $FlowFixMe
        if (Home.instance()) Home.instance().mounted();
        history.push('/');
      })
      .catch((error: Error) => Alert.danger(error.message));
  }
}

class CategoryList extends Component<{ match: { params: { category: string } } }> {
  news: News[] = [];

  render() {
    if (this.news.length === 0) return null;
    return <NewsList news={this.news} />;
  }

  mounted() {
    newsService
      .getCategoryNews(this.props.match.params.category)
      .then(news => {
        this.news = news;
      })
      .catch((error: Error) => Alert.danger(error.message));
  }
}

class RegisterNews extends Component {
  categories: Category[] = [];

  render() {
    if (this.categories.length === 0) return null;
    return <NewsNewForm categories={this.categories} />;
  }

  mounted() {
    categoryService
      .getCategories()
      .then(categories => (this.categories = categories))
      .catch((error: Error) => Alert.danger(error.message));
  }
}

const root = document.getElementById('root');
if (root)
  ReactDOM.render(
    <HashRouter>
      <div>
        <Alert />
        <Menu />
        <Route exact path="/" component={Home} />
        <Route exact path="/news/new" component={RegisterNews} />
        <Route exact path="/news/:category" component={CategoryList} />
        <Route exact path="/news/:category/:id" component={NewsDetails} />
        <Route exact path="/news/:category/:id/edit" component={NewsEdit} />
        <Route exact path="/news/:category/:id/delete" component={NewsDelete} />
      </div>
    </HashRouter>,
    root
  );
