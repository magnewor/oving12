// @flow

import * as React from 'react';
import { Alert, AutoCollapsingNavbar } from '../src/widgets.js';
import { shallow, mount } from 'enzyme';
import { NavbarToggler, NavLink, NavItem } from 'reactstrap';
import { NavLink as RRNavLink } from 'react-router-dom';
import { DeleteConfirmation, NewsList } from '../src/widgets';

// Har brukt en del FlowFixMe på testobjektene, for at de skal aksepteres av flow.

describe('Alert tests', () => {
  const wrapper = shallow(<Alert />);

  it('initially', () => {
    let instance: ?Alert = Alert.instance();
    expect(typeof instance).toEqual('object');
    if (instance) expect(instance.alerts).toEqual([]);

    expect(wrapper.find('button.close')).toHaveLength(0);
  });

  it('after danger', done => {
    Alert.danger('test');

    setTimeout(() => {
      let instance: ?Alert = Alert.instance();
      expect(typeof instance).toEqual('object');
      if (instance) expect(instance.alerts).toEqual([{ text: 'test', type: 'danger' }]);

      expect(wrapper.find('button.close')).toHaveLength(1);

      done();
    });
  });

  it('after clicking close button', () => {
    wrapper.find('button.close').simulate('click');

    let instance: ?Alert = Alert.instance();
    expect(typeof instance).toEqual('object');
    if (instance) expect(instance.alerts).toEqual([]);

    expect(wrapper.find('button.close')).toHaveLength(0);
  });
});

describe('AutoCollapsingNavbar tests', () => {
  const wrapper = shallow(
    <AutoCollapsingNavbar
      categories={[
        // $FlowFixMe
        {
          name: 'One'
        },
        // $FlowFixMe
        {
          name: 'Two'
        }
      ]}
    />
  );

  it('not collapsed initially', () => {
    expect(wrapper.state().isOpen).toEqual(false);
  });

  it('collapse/decollapse working', () => {
    wrapper.find(NavbarToggler).simulate('click');
    expect(wrapper.state().isOpen).toEqual(true);
    wrapper.find(NavbarToggler).simulate('click');
    expect(wrapper.state().isOpen).toEqual(false);
  });

  it('correct categories', () => {
    expect(
      wrapper.containsAllMatchingElements([
        <NavItem className="navItem" key="One">
          <NavLink tag={RRNavLink} className="nav-link" to="/news/one" activeClassName="active">
            One
          </NavLink>
        </NavItem>,
        <NavItem className="navItem" key="Two">
          <NavLink tag={RRNavLink} className="nav-link" to="/news/two" activeClassName="active">
            Two
          </NavLink>
        </NavItem>
      ])
    ).toEqual(true);
  });
});

describe('NewsList tests', () => {
  const wrapper = shallow(
    <NewsList
      news={[
        // $FlowFixMe
        {
          id: 1,
          title: 'TestTitle',
          content: 'Test Content',
          image:
            'http://www.agderposten.no/polopoly_fs/1.2185039.1509436685!/image/708041388.jpg_gen/derivatives/facelift_980/708041388.jpg',
          category: 'TestCategory',
          important: true,
          createdAt: Date.now()
        },
        // $FlowFixMe
        {
          id: 2,
          title: 'TestTitle2',
          content: 'Test Content2',
          image: 'https://surf-norge.no/wp-content/uploads/2017/04/58ff96bd1b721.jpg',
          category: 'TestCategory2',
          important: true,
          createdAt: Date.now()
        }
      ]}
    />
  );

  it('correctly displaying 2 news', () => {
    expect(
      wrapper.containsAllMatchingElements([
        <div className="col-lg-4 col-sm-6" key={1}>
          <RRNavLink exact to={'/news/testcategory/1'}>
            <img
              className="img-thumbnail"
              src="http://www.agderposten.no/polopoly_fs/1.2185039.1509436685!/image/708041388.jpg_gen/derivatives/facelift_980/708041388.jpg"
            />
          </RRNavLink>
          <RRNavLink activeStyle={{ color: 'darkblue' }} exact to={'/news/testcategory/1'}>
            TestTitle
          </RRNavLink>
        </div>,
        <div className="col-lg-4 col-sm-6" key={2}>
          <RRNavLink exact to={'/news/testcategory2/2'}>
            <img className="img-thumbnail" src="https://surf-norge.no/wp-content/uploads/2017/04/58ff96bd1b721.jpg" />
          </RRNavLink>
          <RRNavLink activeStyle={{ color: 'darkblue' }} exact to={'/news/testcategory2/2'}>
            TestTitle2
          </RRNavLink>
        </div>
      ])
    ).toEqual(true);
  });
});

describe('DeleteConfirmation tests', () => {
  const mockdelete = jest.fn();
  const wrapper = mount(
    <DeleteConfirmation
      news={
        // $FlowFixMe
        {
          id: 1,
          title: 'TestTitle',
          content: 'Test Content',
          image:
            'http://www.agderposten.no/polopoly_fs/1.2185039.1509436685!/image/708041388.jpg_gen/derivatives/facelift_980/708041388.jpg',
          category: 'TestCategory',
          important: true
        }
      }
      deletemethod={mockdelete}
    />
  );

  it('canceling delete works', () => {
    wrapper.find('a').simulate('click');
    expect(mockdelete).toHaveBeenCalledTimes(0);
  });

  it('delete works', () => {
    wrapper.find('button').simulate('click');
    expect(mockdelete).toHaveBeenCalled();
  });
});
