// @flow

import { Category, Comment, News, sync } from '../src/models.js';

beforeAll(async () => {
  await sync;
});

describe('Database test', () => {
  it('Categories.findAll() returns correct data', async () => {
    let categories = await Category.findAll();

    expect(
      categories.map(category => category.toJSON()).map(category => ({
        name: category.name
      }))
    ).toEqual([
      {
        name: 'Kultur'
      },
      {
        name: 'Nyheter'
      },
      {
        name: 'Sport'
      }
    ]);
  });

  it('News.findAll() returns correct data', async () => {
    let news = await News.findAll();
    let loremipsum =
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus rhoncus dictum risus, a ' +
      'sagittis lectus malesuada nec. Fusce ac risus porttitor, placerat odio vel, aliquet turpis. Cras ut elit ' +
      'vitae erat sollicitudin bibendum. Etiam a dolor venenatis, iaculis dui hendrerit, semper nisi. Phasellus ' +
      'pellentesque convallis semper. Nulla vitae lorem vel leo molestie sollicitudin. Aliquam quis lacus a sem ' +
      'gravida eleifend. Etiam iaculis libero vel sem bibendum rhoncus. Morbi lacinia eleifend purus in mollis. Cras ' +
      'iaculis accumsan mauris quis aliquet. Etiam congue a nisl nec porttitor. Phasellus viverra ante non mauris ' +
      'scelerisque pharetra. Praesent quis sollicitudin erat. Maecenas massa odio, sagittis in tellus nec, efficitur ' +
      'scelerisque est. Aenean luctus lorem vel leo aliquam, id efficitur justo tempus.' +
      'Morbi id dui non enim interdum euismod. Quisque dapibus congue risus, non cursus nisi maximus quis. Integer ' +
      'congue, libero eu pellentesque blandit, libero leo ornare nisi, non finibus lorem ligula in erat. Class aptent ' +
      'taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris et leo eu mi dictum ' +
      'ultricies eu sed felis. Cras vitae eleifend mi, a pretium erat. Morbi at laoreet ipsum. Donec mollis velit nec ' +
      'risus maximus scelerisque. Donec efficitur ipsum sed purus ultricies, a egestas mauris malesuada. Phasellus ut ' +
      'vestibulum mi, id feugiat orci. Vestibulum aliquet gravida semper. Etiam tortor leo, malesuada in accumsan ' +
      'vitae, cursus ac velit. Maecenas at sapien consectetur, ullamcorper ex vel, tincidunt justo. Nulla eu urna ' +
      'id lectus volutpat pulvinar sed at est. Ut ut maximus velit, non commodo erat. Nullam massa lorem, fringilla ' +
      'at nulla at, fermentum porta elit. Praesent massa risus, pretium sed imperdiet eu, molestie a lorem. Phasellus ' +
      'vitae lacus hendrerit, hendrerit purus id, interdum est. Mauris tincidunt leo risus, in consequat justo ' +
      'tincidunt eget. Donec tincidunt nisi est, ut rhoncus eros accumsan convallis. Nam non mattis eros, sit amet ' +
      'viverra eros. Ut auctor feugiat odio, quis ornare felis. Etiam id sem sit amet ligula euismod gravida. Sed sed ' +
      'bibendum ex, eu mattis elit. Nunc eu orci leo. Vestibulum suscipit quam sed dolor pretium, at elementum lectus ' +
      'elementum. Nunc ac leo consequat libero blandit ornare. Vestibulum quis nunc ligula. Quisque posuere libero ' +
      'ac aliquet imperdiet. Donec eu elementum mi, vitae eleifend sapien. Sed id sollicitudin tortor, eu bibendum' +
      ' ligula. Donec varius magna ut mi imperdiet hendrerit. Vestibulum purus massa, semper vehicula ex at,' +
      ' vestibulum feugiat libero. In justo urna, consequat vitae arcu in, bibendum molestie nibh. Nullam bibendum ' +
      'hendrerit ligula vitae rhoncus. Donec mattis feugiat urna quis pulvinar. In quis felis dolor. Aenean a aliquet ' +
      'odio, et iaculis metus. Aenean lobortis hendrerit venenatis. Pellentesque a sollicitudin est. Vestibulum ' +
      'ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla sit amet felis fringilla, ' +
      'dictum lacus quis, egestas sapien. Praesent vitae lectus dui. Aenean vulputate blandit nisl sed pharetra. Sed ' +
      'justo velit, placerat ultricies sodales consequat, ornare a dolor. Pellentesque auctor ultrices dignissim. ' +
      'Praesent eu arcu mattis, vehicula metus nec, bibendum odio. Vivamus pellentesque vitae turpis dapibus congue. ' +
      'Curabitur commodo dolor mauris, et egestas elit viverra et. Nullam semper augue at urna vehicula condimentum. ' +
      'Nam congue, ligula tincidunt molestie aliquet, neque nunc egestas mauris, quis porta sapien erat id lectus. ' +
      'Nunc eleifend, ex sed pellentesque lacinia, lorem elit pulvinar nibh, at blandit libero nulla a risus. Phasellus ' +
      'rutrum ullamcorper eros, nec mollis elit condimentum at. Suspendisse sed pretium arcu, efficitur ultricies ' +
      'felis. Nunc a neque vel libero imperdiet consectetur sit amet sed sapien.';

    expect(
      news.map(news => news.toJSON()).map(news => ({
        title: news.title,
        content: news.content,
        image: news.image,
        important: news.important,
        category: news.category
      }))
    ).toEqual([
      {
        title: 'TestKultur',
        content: loremipsum,
        image: 'https://images.pexels.com/photos/20787/pexels-photo.jpg?auto=compress&cs=tinysrgb&h=350',
        important: true,
        category: 'Kultur'
      },
      {
        title: 'TestSport',
        content: loremipsum,
        image: 'https://www.fotball.no/imagevault/publishedmedia/qfs5k3lc6hyy63139fln/Sommerfotball.jpg',
        important: false,
        category: 'Sport'
      },
      {
        title: 'TestNyhet',
        content: loremipsum,
        image:
          'http://www.agderposten.no/polopoly_fs/1.2185039.1509436685!/image/708041388.jpg_gen/derivatives/facelift_980/708041388.jpg',
        important: true,
        category: 'Nyheter'
      }
    ]);
  });

  it('News.create, .findOne, .update and .destroy works', async () => {
    await News.create({
      title: 'TestTitle',
      content: 'TestContent',
      image: 'https://surf-norge.no/wp-content/uploads/2017/04/58ff96bd1b721.jpg',
      category: 'Kultur',
      important: false
    });

    // $FlowFixMe
    let newsFindOne = await News.findOne({ where: { id: Number(4) } });
    let array = [newsFindOne];
    expect(
      array.map(news => news.toJSON()).map(news => ({
        title: news.title,
        content: news.content,
        image: news.image,
        important: news.important,
        category: news.category
      }))
    ).toEqual([
      {
        title: 'TestTitle',
        content: 'TestContent',
        image: 'https://surf-norge.no/wp-content/uploads/2017/04/58ff96bd1b721.jpg',
        category: 'Kultur',
        important: false
      }
    ]);

    News.update(
      {
        title: 'TestTitle2',
        content: 'TestContent2',
        image: 'http://img.gfx.no/615/615789/bilde1.jpg',
        category: 'Sport',
        important: true
      },
      { where: { id: 4 } }
    ).then(() => {
      expect(
        array.map(news => news.toJSON()).map(news => ({
          title: news.title,
          content: news.content,
          image: news.image,
          important: news.important,
          category: news.category
        }))
      ).toEqual([
        {
          title: 'TestTitle2',
          content: 'TestContent2',
          image: 'http://img.gfx.no/615/615789/bilde1.jpg',
          category: 'Sport',
          important: true
        }
      ]);
    });

    News.destroy({ where: { id: 4 } }).then(() => {
      // $FlowFixMe
      expect(News.findAll().length).toEqual(3);
    });
  });

  it('Comment.findAll() returns correct data', async () => {
    let comments = await Comment.findAll();

    expect(
      comments.map(comment => comment.toJSON()).map(comment => ({
        nickname: comment.nickname,
        content: comment.content,
        newsId: comment.newsId
      }))
    ).toEqual([
      {
        nickname: 'Ola',
        content: 'Dette var en fin katt',
        newsId: 1
      }
    ]);
  });

  it('Comment.create() and Comment.findAll(where) works', async () => {
    Comment.create({
      nickname: 'Test',
      content: 'asdlfkjsadlfkjsadf',
      newsId: 2
    }).then(() => {
      expect(
        Comment.findAll({
          where: {
            newsId: Number(2)
          }
        })
      ).toEqual(1);
    });
  });
});
