// @flow

import Sequelize from 'sequelize';
import type { Model } from 'sequelize';

let sequelize = new Sequelize(
  process.env.CI ? 'School' : 'magnewor',
  process.env.CI ? 'root' : 'magnewor',
  process.env.CI ? '' : 'O1hImhoD',
  {
    host: process.env.CI ? 'mysql' : 'mysql.stud.iie.ntnu.no', // The host is 'mysql' when running in gitlab CI
    dialect: 'mysql',

    pool: {
      max: 5,
      min: 0,
      acquire: 30000,
      idle: 10000
    }
  }
);

export let News: Class<
  Model<{ id?: number, title: string, content: string, image: string, important: boolean, category?: string }>
> = sequelize.define('News', {
  id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
  title: Sequelize.STRING,
  content: Sequelize.TEXT,
  image: Sequelize.TEXT,
  important: Sequelize.BOOLEAN
});

export let Category: Class<Model<{ name: string }>> = sequelize.define(
  'Category',
  {
    name: { type: Sequelize.STRING, primaryKey: true }
  },
  {
    timestamps: false
  }
);

export let Comment: Class<
  Model<{ id?: number, nickname: string, content: string, newsId?: number }>
> = sequelize.define('Comment', {
  id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
  nickname: { type: Sequelize.STRING },
  content: { type: Sequelize.TEXT }
});

Category.hasMany(News, { foreignKey: { name: 'category', allowNull: false } });
News.hasMany(Comment, { foreignKey: { name: 'newsId', allowNull: false } });

// Drop tables and create test data when not in production environment
let production = process.env.NODE_ENV === 'production';
let loremipsum = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus rhoncus dictum risus, a ' +
  'sagittis lectus malesuada nec. Fusce ac risus porttitor, placerat odio vel, aliquet turpis. Cras ut elit ' +
  'vitae erat sollicitudin bibendum. Etiam a dolor venenatis, iaculis dui hendrerit, semper nisi. Phasellus ' +
  'pellentesque convallis semper. Nulla vitae lorem vel leo molestie sollicitudin. Aliquam quis lacus a sem ' +
  'gravida eleifend. Etiam iaculis libero vel sem bibendum rhoncus. Morbi lacinia eleifend purus in mollis. Cras ' +
  'iaculis accumsan mauris quis aliquet. Etiam congue a nisl nec porttitor. Phasellus viverra ante non mauris ' +
  'scelerisque pharetra. Praesent quis sollicitudin erat. Maecenas massa odio, sagittis in tellus nec, efficitur ' +
  'scelerisque est. Aenean luctus lorem vel leo aliquam, id efficitur justo tempus.' +
  'Morbi id dui non enim interdum euismod. Quisque dapibus congue risus, non cursus nisi maximus quis. Integer ' +
  'congue, libero eu pellentesque blandit, libero leo ornare nisi, non finibus lorem ligula in erat. Class aptent ' +
  'taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris et leo eu mi dictum ' +
  'ultricies eu sed felis. Cras vitae eleifend mi, a pretium erat. Morbi at laoreet ipsum. Donec mollis velit nec ' +
  'risus maximus scelerisque. Donec efficitur ipsum sed purus ultricies, a egestas mauris malesuada. Phasellus ut ' +
  'vestibulum mi, id feugiat orci. Vestibulum aliquet gravida semper. Etiam tortor leo, malesuada in accumsan ' +
  'vitae, cursus ac velit. Maecenas at sapien consectetur, ullamcorper ex vel, tincidunt justo. Nulla eu urna ' +
  'id lectus volutpat pulvinar sed at est. Ut ut maximus velit, non commodo erat. Nullam massa lorem, fringilla ' +
  'at nulla at, fermentum porta elit. Praesent massa risus, pretium sed imperdiet eu, molestie a lorem. Phasellus ' +
  'vitae lacus hendrerit, hendrerit purus id, interdum est. Mauris tincidunt leo risus, in consequat justo ' +
  'tincidunt eget. Donec tincidunt nisi est, ut rhoncus eros accumsan convallis. Nam non mattis eros, sit amet ' +
  'viverra eros. Ut auctor feugiat odio, quis ornare felis. Etiam id sem sit amet ligula euismod gravida. Sed sed ' +
  'bibendum ex, eu mattis elit. Nunc eu orci leo. Vestibulum suscipit quam sed dolor pretium, at elementum lectus ' +
  'elementum. Nunc ac leo consequat libero blandit ornare. Vestibulum quis nunc ligula. Quisque posuere libero ' +
  'ac aliquet imperdiet. Donec eu elementum mi, vitae eleifend sapien. Sed id sollicitudin tortor, eu bibendum' +
  ' ligula. Donec varius magna ut mi imperdiet hendrerit. Vestibulum purus massa, semper vehicula ex at,' +
  ' vestibulum feugiat libero. In justo urna, consequat vitae arcu in, bibendum molestie nibh. Nullam bibendum ' +
  'hendrerit ligula vitae rhoncus. Donec mattis feugiat urna quis pulvinar. In quis felis dolor. Aenean a aliquet ' +
  'odio, et iaculis metus. Aenean lobortis hendrerit venenatis. Pellentesque a sollicitudin est. Vestibulum ' +
  'ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla sit amet felis fringilla, ' +
  'dictum lacus quis, egestas sapien. Praesent vitae lectus dui. Aenean vulputate blandit nisl sed pharetra. Sed ' +
  'justo velit, placerat ultricies sodales consequat, ornare a dolor. Pellentesque auctor ultrices dignissim. ' +
  'Praesent eu arcu mattis, vehicula metus nec, bibendum odio. Vivamus pellentesque vitae turpis dapibus congue. ' +
  'Curabitur commodo dolor mauris, et egestas elit viverra et. Nullam semper augue at urna vehicula condimentum. ' +
  'Nam congue, ligula tincidunt molestie aliquet, neque nunc egestas mauris, quis porta sapien erat id lectus. ' +
  'Nunc eleifend, ex sed pellentesque lacinia, lorem elit pulvinar nibh, at blandit libero nulla a risus. Phasellus ' +
  'rutrum ullamcorper eros, nec mollis elit condimentum at. Suspendisse sed pretium arcu, efficitur ultricies ' +
  'felis. Nunc a neque vel libero imperdiet consectetur sit amet sed sapien.';
// The sync promise can be used to wait for the database to be ready (for instance in your tests)
export let sync = sequelize.sync({ force: !production }).then(() => {
  if (!production)
    return Category.create({
      name: 'Kultur'
    })
      .then(() =>
        Category.create({
          name: 'Sport'
        })
      )
      .then(() =>
        Category.create({
          name: 'Nyheter'
        })
      )
      .then(() => {
        News.create({
          title: 'TestKultur',
          content: loremipsum,
          image: 'https://images.pexels.com/photos/20787/pexels-photo.jpg?auto=compress&cs=tinysrgb&h=350',
          important: true,
          category: 'Kultur'
        });
      })
      .then(() => {
        News.create({
          title: 'TestSport',
          content: loremipsum,
          image: 'https://www.fotball.no/imagevault/publishedmedia/qfs5k3lc6hyy63139fln/Sommerfotball.jpg',
          important: false,
          category: 'Sport'
        });
      })
      .then(() => {
        News.create({
          title: 'TestNyhet',
          content: loremipsum,
          image: 'http://www.agderposten.no/polopoly_fs/1.2185039.1509436685!/image/708041388.jpg_gen/derivatives/facelift_980/708041388.jpg',
          important: true,
          category: 'Nyheter'
        }).then(() =>
          Comment.create({
            nickname: 'Ola',
            content: 'Dette var en fin katt',
            newsId: 1
          })
        );
      });
});
