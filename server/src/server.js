// @flow

import express from 'express';
import path from 'path';
import reload from 'reload';
import fs from 'fs';
import { News, Category, Comment } from './models.js';

type Request = express$Request;
type Response = express$Response;

const public_path = path.join(__dirname, '/../../client/public');

let app = express();

app.use(express.static(public_path));
app.use(express.json()); // For parsing application/json

app.get('/news', (req: Request, res: Response) => {
  return News.findAll({
    order: [['createdAt', 'DESC']]
  }).then(news => res.send(news));
});

app.get('/news/:category/:id', (req: Request, res: Response) => {
  return News.findOne({ where: { id: Number(req.params.id) } }).then(
    news => (news ? res.send(news) : res.sendStatus(404))
  );
});

app.post('/news', (req: Request, res: Response) => {
  if (
    !req.body ||
    typeof req.body.title != 'string' ||
    typeof req.body.content != 'string' ||
    typeof req.body.image != 'string' ||
    typeof req.body.category != 'string' ||
    typeof req.body.important != 'boolean'
  )
    return res.sendStatus(400);

  return News.create({
    title: req.body.title,
    content: req.body.content,
    image: req.body.image,
    category: req.body.category,
    important: req.body.important
  }).then(news => res.send(news));
});

app.delete('/news/:category/:id', (req: Request, res: Response) => {
  return News.destroy({ where: { id: Number(req.params.id) } }).then(
    result => (result ? res.send() : res.status(500).send())
  );
});

app.put('/news/:category/:id', (req: Request, res: Response) => {
  if (
    !req.body ||
    typeof req.body.id != 'number' ||
    typeof req.body.title != 'string' ||
    typeof req.body.content != 'string' ||
    typeof req.body.image != 'string' ||
    typeof req.body.category != 'string' ||
    typeof req.body.important != 'boolean'
  )
    return res.sendStatus(400);

  return News.update(
    {
      title: req.body.title,
      content: req.body.content,
      image: req.body.image,
      category: req.body.category,
      important: req.body.important
    },
    { where: { id: req.body.id } }
  ).then(count => (count ? res.sendStatus(200) : res.sendStatus(404)));
});

app.get('/news/:category', (req: Request, res: Response) => {
  return News.findAll({
    where: {
      category: req.params.category
    },
    order: [['createdAt', 'DESC']]
  }).then(news => res.send(news));
});

app.get('/categories', (req: Request, res: Response) => {
  return Category.findAll().then(category => res.send(category));
});

app.get('/comments/:id', (req: Request, res: Response) => {
  return Comment.findAll({
    where: {
      newsId: Number(req.params.id)
    },
    order: [['createdAt', 'DESC']]
  }).then(comments => (comments ? res.send(comments) : res.sendStatus(404)));
});

app.post('/comments', (req: Request, res: Response) => {
  if (
    !req.body ||
    typeof req.body.nickname != 'string' ||
    typeof req.body.content != 'string' ||
    typeof req.body.newsId != 'number'
  )
    return res.sendStatus(400);

  return Comment.create({
    nickname: req.body.nickname,
    content: req.body.content,
    newsId: req.body.newsId
  }).then(comment => res.json(comment));
});

// Hot reload application when not in production environment
if (process.env.NODE_ENV !== 'production') {
  let reloadServer = reload(app);
  fs.watch(public_path, () => reloadServer.reload());
}

// The listen promise can be used to wait for the web server to start (for instance in your tests)
export let listen = new Promise<void>((resolve, reject) => {
  app.listen(3000, error => {
    if (error) reject(error.message);
    console.log('Server started');
    resolve();
  });
});
